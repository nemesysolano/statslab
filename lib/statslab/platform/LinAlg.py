import numpy

class LinAlg(object):
    @classmethod
    def all_close(clazz, A, b, x):

        return numpy.allclose(A @ x - b, numpy.zeros(len(b)))
