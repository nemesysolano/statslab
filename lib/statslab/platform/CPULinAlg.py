#
# Motivation: https://stackoverflow.com/questions/41567895/will-scikit-learn-utilize-gpu
#
from scipy.linalg import cho_factor, cho_solve
from .LinAlg import LinAlg

class CPULinAlg(LinAlg):
    def cholesky_solve(self,  A, b):        
        c, low = cho_factor(A)
        x = cho_solve((c, low), b, overwrite_b = True)        
        return x