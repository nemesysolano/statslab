class ReductorFactory(object):
    
    @classmethod
    def cpu_reductor(clazz):
        from .CPUReductor import CPUReductor
        return CPUReductor()

    @classmethod
    def gpu_reductor(clazz):
        from .GPUReductor import GPUReductor
        return GPUReductor()

    @classmethod
    def reductor(clazz):
        import GPUtil
        
        available = GPUtil.getAvailable()    
        if(len(available)) > 0:
            return ReductorFactory.gpu_reductor()

        return ReductorFactory.cpu_reductor()