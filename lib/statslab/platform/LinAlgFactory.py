
class LinAlgFactory(object):

    @classmethod
    def gpu_linalg(clazz):
        from .GPULinAlg import GPULinAlg
        return GPULinAlg()

    @classmethod
    def cpu_linalg(clazz):
        from .CPULinAlg import CPULinAlg
        return CPULinAlg()

    @classmethod
    def linalg(clazz):
        import GPUtil
        
        available = GPUtil.getAvailable()    
        if(len(available)) > 0:
            return LinAlgFactory.cpu_linalg()

        return LinAlgFactory.cpu_linalg()