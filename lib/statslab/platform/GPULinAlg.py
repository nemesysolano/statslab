import skcuda.linalg as linalg
import skcuda.misc
from .Arrays import Arrays
from .LinAlg import LinAlg

skcuda.misc.init()
linalg.init()

class GPULinAlg(LinAlg):
    
    def cholesky_solve(self, a, B): # Better performance is achieved if caller ensures that a & B are gpu arrays.
        A = Arrays.to_array(a)
        b = Arrays.to_array(B)
        linalg.cho_solve(A, b)
        return b.get()