from statslab.platform import LinAlg
from statslab.platform import LinAlgFactory
import numpy

def cpu_test(A,b, linalg):
    B = numpy.copy(b)
    x = linalg.cholesky_solve(A,b)
    print(LinAlg.all_close(A,B,x))


if __name__ == "__main__":
    A = numpy.array([[9, 3, 1, 5], [3, 7, 5, 1], [1, 5, 9, 2], [5, 1, 2, 6]])
    b = numpy.array((1,2,1,3))
    cpu_test(A,b, LinAlgFactory.cpu_linalg())
    cpu_test(A,b, LinAlgFactory.gpu_linalg())
    