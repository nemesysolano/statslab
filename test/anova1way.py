from statslab.anova import ANOVA
import pandas
import sys

if __name__ == "__main__":
    input_file = sys.argv[1]
    dataset = pandas.read_csv(input_file,  infer_datetime_format = True)
    samples = tuple(map(lambda c: dataset[c].dropna(), dataset.columns))    
    print(ANOVA.fully_randomized(samples))